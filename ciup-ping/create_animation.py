import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import datetime as dt

INPUT_FILE = "./data.csv"
OUTPUT_FILE = "./animation.gif"

DAYS_RANGE = 7  # number of days to aggregate
START_DATE = dt.date(year=2019, month=11, day=4)
END_DATE = dt.date(year=2020, month=4, day=9)
NB_DAYS = (END_DATE - START_DATE).days
DAYS = [START_DATE + dt.timedelta(i) for i in range(NB_DAYS)]


def to_datetime(date):
    return dt.datetime(date.year, date.month, date.day)


def get_histogram(begin):
    end = begin + dt.timedelta(DAYS_RANGE)
    mask = (df["date"] > to_datetime(begin)) & (df["date"] < to_datetime(end))
    return np.histogram2d(
        df.loc[mask]["date"].apply(lambda x: 60 * x.hour + x.minute),
        df.loc[mask]["ping"],
        bins=(np.linspace(0, 60 * 24, 48), np.logspace(0.15, np.log10(25), 48)),
    )[0].T


def set_frame(day):
    im.set_data(get_histogram(day))
    plt.title("Internet connection at CiuP\n" + day.strftime("%B %Y"))


# Init global objects
df = pd.read_csv(INPUT_FILE, sep="\t", parse_dates=["date"]).dropna()
fig = plt.figure(figsize=(5, 5))
im = plt.imshow(get_histogram(START_DATE), origin="lower", cmap=plt.get_cmap("magma"))
animation = ani.FuncAnimation(fig, set_frame, DAYS, interval=100)

# Y Axis
plt.ylabel("Ping\n(log scale)")
plt.yticks([])

# X Axis
labels = [f"{i}h" for i in range(0, 25, 2)]
ticks = np.arange(0, 48, step=4)
plt.xticks(ticks, labels)

# Output
animation.save("./animation.gif", writer="imagemagick", fps=10)
