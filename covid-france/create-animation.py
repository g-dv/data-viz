import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.animation as ani


def get_population(department):
    return {
        "59": 595536,
        "75": 229621,
        "13": 993177,
        "69": 779845,
        "92": 591403,
        "93": 552482,
        "33": 505517,
        "62": 465205,
        "78": 418484,
        "77": 365200,
        "94": 354005,
        "44": 328620,
        "31": 298562,
        "76": 254609,
        "91": 253931,
        "38": 235387,
        "95": 194681,
        "67": 109460,
        "34": 1092331,
        "06": 1080771,
        "57": 1046873,
        "83": 1028583,
        "35": 1019923,
        "29": 903921,
        "974": 835103,
        "60": 815400,
        "49": 800191,
        "74": 769677,
        "68": 758723,
        "42": 756715,
        "56": 737778,
        "30": 733201,
        "54": 731004,
        "14": 689945,
        "45": 665587,
        "64": 664057,
        "85": 655506,
        "63": 640999,
        "17": 633417,
        "01": 619497,
        "37": 600252,
        "22": 597085,
        "27": 595043,
        "80": 571675,
        "51": 569999,
        "72": 569035,
        "71": 556222,
        "84": 549949,
        "02": 540067,
        "25": 533320,
        "21": 529761,
        "50": 499919,
        "26": 494712,
        "66": 462705,
        "28": 432967,
        "86": 431248,
        "73": 423715,
        "24": 416909,
        "971": 402119,
        "40": 397226,
        "972": 385551,
        "81": 381927,
        "87": 375856,
        "88": 375226,
        "79": 371632,
        "11": 364877,
        "16": 353482,
        "03": 343431,
        "89": 341483,
        "47": 333180,
        "41": 332001,
        "07": 320379,
        "18": 311650,
        "53": 307500,
        "10": 306581,
        "61": 288848,
        "08": 280907,
        "12": 277740,
        "39": 260502,
        "82": 250342,
        "973": 244118,
        "19": 240781,
        "70": 238956,
        "65": 228868,
        "36": 228091,
        "43": 226203,
        "58": 215221,
        "976": 212645,
        "55": 192094,
        "32": 190276,
        "52": 181521,
        "46": 173758,
        "2B": 170974,
        "04": 161916,
        "09": 152684,
        "2A": 149234,
        "15": 147035,
        "90": 144318,
        "05": 139279,
        "23": 120872,
        "48": 76607,
    }[department]


def get_values(day):
    hospitalizations = df.loc[df["jour"] == day.strftime("%Y-%m-%d")]
    get_row = lambda x: hospitalizations.loc[hospitalizations["dep"] == x]
    # divide by the population to get the number of cases/inhabitant
    return [(int(get_row(d)["rea"]) / get_population(d)) for d in departments["code"]]


def set_frame(day):
    print(day.strftime("%Y-%m-%d"))
    global ax
    departments["n_hospitalizations"] = get_values(day)
    # plot
    ax = departments.plot(
        column="n_hospitalizations",
        ax=ax,
        scheme="user_defined",
        classification_kwds={"bins": bins},
    )
    title = day.strftime("%B %d")
    fig.suptitle(title, y=0.01, x=0.99, ha="right", va="bottom", size=28)


# read data
departments = gpd.read_file("departments.geojson").to_crs("epsg:3395")
df = pd.read_csv(f"hospitalizations-2020-09-26.csv", sep=";")
df = df.loc[df["sexe"] == 0]

# init figure
fig, ax = plt.subplots(figsize=(10, 10))
plt.axis("off")
scale = np.linspace(5e-6, 4.5e-5, 9)
bins = np.append(scale, scale * 10)

# animate
dates = pd.date_range(start=df["jour"].min(), end=df["jour"].max())
animation = ani.FuncAnimation(fig, set_frame, dates)
animation.save("./animation.mp4", writer="ffmpeg", fps=10)
