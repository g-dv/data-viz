## Table of contents

1. [Ping from the "Cité internationale universitaire de Paris"](#1-ping-from-the-cité-internationale-universitaire-de-paris)
2. [Covid-19 in France](#2-covid-19-in-france)

## 1. Ping from the "Cité internationale universitaire de Paris"

### 1.1. Animation

![Internet connection at CiuP](ciup-ping/animation.gif)

### 1.2. Note

I monitored the CiuP's internet connection quality over time.

As the animation shows:

- In 2019, residents would experience slowdowns between 7PM and 1AM.
- Something happened in late January that fixed the issue and ensured a stable internet connection.

### 1.3. Focus

![Internet connection at CiuP](ciup-ping/october-2019.png)

## 2. Covid-19 in France

### 2.1. Animation

![Covid-19 in France](covid-france/animation.gif)

### 2.2. Note

Number hospitalizations divided by the number of inhabitants for each department of France.

The color is represented on a logarithmic scale ($`log_2`$).
